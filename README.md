## What does it do

Bash script which makes git pull, copy files and shows git status, in every folder avaialable both in **origin path** and **target path**.

## Installation

If you want to be able run command from any place from terminal it should be placed in **/usr/local/bin**.

## Configuration:

You need to change **origin path** and **target path** in script files.
